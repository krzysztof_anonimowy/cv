import QtQuick 2.4

//much better scaling, easy color changing of the image
//useful especially on mobile devices with different resolutions

Item {
  property int size: 16
  property string icon
  property alias color: imageText.color
  width: imageText.implicitWidth
  height: imageText.implicitHeight

  onIconChanged: setIcon(icon)
  function setIcon(i) {
    var iconsMap = {
      "projects" : "\uE0CA",
      "phone" : "\uE0A6",
      "star" : "\uE0C2",
      "education" : "\uE021",
      "eye" : "\uE065",
      "arrow" : "\uE097"
    };

    if (iconsMap[i] !== undefined)
      imageText.text = iconsMap[i]
  }

  FontLoader {
    id: fontLoader
    source: "qrc:///images/icons.ttf"
  }

  Text {
    id: imageText
    anchors.centerIn: parent
    font.family: fontLoader.name
    font.pixelSize: parent.size
  }
}
