import QtQuick 2.4

MouseArea {
  width: 32; height: 32
  property string source: ""
  property bool checked: false
  cursorShape: Qt.PointingHandCursor
  hoverEnabled: true
  onClicked: checked = true

  Image {
    anchors.fill: parent
    source: parent.source
    opacity: parent.checked || parent.containsMouse ? 1 : 0.8
  }
}
