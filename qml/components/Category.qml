import QtQuick 2.4
import "qrc:///qml/components" as CWorks

Row {
  id: item
  property bool center: false
  property alias icon: fontIcon.icon
  spacing: center ? 0 : 20

  CWorks.VerticalLine {
    anchors.verticalCenter: parent.verticalCenter
    width: (item.width - 2 * item.spacing - dot.width) / 2
    visible: item.center
  }

  Rectangle {
    id: dot
    anchors.verticalCenter: parent.verticalCenter
    width: 36; height: 36
    radius: width / 2
    color: "#1F2120"

    CWorks.FontIcon {
      id: fontIcon
      anchors.centerIn: parent
      color: "#FFFFFF"
    }
  }

  CWorks.VerticalLine {
    anchors.verticalCenter: parent.verticalCenter
    width: item.width - x
  }
}
