import QtQuick 2.4
import "qrc:///qml/components" as CWorks
import "qrc:///qml/data" as CWorks

Rectangle {
  color: "#EFEFEF"

  Connections {
    target: Translator
    onTrStringChanged: {
      projectsModel.reset()
      educationModel.reset()

      educationRepeater.model = 0
      educationRepeater.model = educationModel
    }
  }

  Flickable {
    anchors.fill: parent
    contentHeight: rootColumn.height + 40

    Column {
      id: rootColumn
      anchors.left: parent.left
      anchors.right: parent.right
      anchors.top: parent.top
      anchors.margins: 20
      spacing: 20

      CWorks.Category {
        anchors.left: parent.left
        anchors.right: parent.right
        icon: "projects"
      }

      Repeater {
        id: projectsRepeater
        model: CWorks.ProjectsList {
          id: projectsModel
        }

        delegate: MouseArea {
          width: rootColumn.width
          height: projectRow.height
          hoverEnabled: true

          Rectangle {
            anchors.fill: parent
            radius: 5
            color: "#DDDDDD"
            opacity: parent.containsMouse ? 1 : 0

            Behavior on opacity {
              NumberAnimation { duration: 300; easing.type: Easing.InOutQuad }
            }
          }

          Row {
            id: projectRow
            width: rootColumn.width

            Column {
              width: 200
              spacing: 2

              CWorks.Header1 {
                anchors.left: parent.left
                anchors.right: parent.right
                text: position
              }

              CWorks.Header2 {
                anchors.left: parent.left
                anchors.right: parent.right
                text: company
                Component.onCompleted: if(company === "") height = 0
              }

              CWorks.Text {
                anchors.left: parent.left
                anchors.right: parent.right
                text: date
              }
            }

            CWorks.Text {
              width: parent.width - x
              text: description
            }
          }
        }
      }

      CWorks.Category {
        anchors.left: parent.left
        anchors.right: parent.right
        icon: "education"
      }

      Repeater {
        id: educationRepeater
        model: CWorks.EducationList {
          id: educationModel
        }

        delegate: Row {
          width: rootColumn.width

          Column {
            width: 200
            spacing: 2

            CWorks.Header2 {
              anchors.left: parent.left
              anchors.right: parent.right
              text: college
            }

            CWorks.Text {
              anchors.left: parent.left
              anchors.right: parent.right
              text: date
            }
          }

          Column {
            width: parent.width - x

            CWorks.Header1 {
              anchors.left: parent.left
              anchors.right: parent.right
              text: direction
            }

            CWorks.Text {
              anchors.left: parent.left
              anchors.right: parent.right
              text: specialization
            }
          }
        }
      }

      Row {
        anchors.left: parent.left
        anchors.right: parent.right
        spacing: 20

        Column {
          width: (parent.width - parent.spacing) / 2
          spacing: 20

          CWorks.Category {
            anchors.left: parent.left
            anchors.right: parent.right
            icon: "star"
          }

          CWorks.Header1 {
            anchors.left: parent.left
            anchors.right: parent.right
            text: qsTr("SKILLS") + Translator.trString
          }

          CWorks.SkillLevel {
            anchors.left: parent.left
            anchors.right: parent.right
            text: "C++"
            level: 4
          }

          CWorks.SkillLevel {
            anchors.left: parent.left
            anchors.right: parent.right
            text: "Qt/QML"
            level: 4
          }

          CWorks.SkillLevel {
            anchors.left: parent.left
            anchors.right: parent.right
            text: "Javascript"
            level: 3
          }

          CWorks.SkillLevel {
            anchors.left: parent.left
            anchors.right: parent.right
            text: "Typescript"
            level: 3
          }
        }

        Column {
          width: parent.width - x
          spacing: 20

          CWorks.Category {
            anchors.left: parent.left
            anchors.right: parent.right
            icon: "eye"
          }

          CWorks.Header1 {
            anchors.left: parent.left
            anchors.right: parent.right
            text: qsTr("INTERESTS") + Translator.trString
          }

          CWorks.Text {
            anchors.left: parent.left
            anchors.right: parent.right
            text: "openCV"
          }

          CWorks.Text {
            anchors.left: parent.left
            anchors.right: parent.right
            text: qsTr("games developing") + Translator.trString
          }
        }
      }
    }
  }
}
