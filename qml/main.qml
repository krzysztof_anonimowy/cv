import QtQuick 2.4
import QtQuick.Window 2.0
import "qrc:///qml/components" as CWorks

Window {
  id: window
  visible: true
  minimumWidth: 768; maximumWidth: 768
  minimumHeight: 800; maximumHeight: 800
  height: 800; width: 768
  title: "CV"

  Details {
    id: details
    anchors.top: parent.top
    anchors.bottom: parent.bottom
    anchors.right: parent.right
    anchors.left: basic.right
  }

  Basic {
    id: basic
    anchors.top: parent.top
    anchors.bottom: parent.bottom
    anchors.left: parent.left
  }
}
