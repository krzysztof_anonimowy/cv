import QtQuick 2.4

Text {
  color: "#1F2120"
  font.capitalization: Font.AllUppercase
  font.family: "Arial"
  font.bold: true
  font.pixelSize: 13
  wrapMode: Text.Wrap
}
