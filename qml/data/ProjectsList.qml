import QtQuick 2.4

ListModel {
  id: model
  Component.onCompleted: reset()

  //ListElement won't allow us to use javascript functions.
  //so we won't be able to translate these texts in common way
  //QT_TR_NOOP doesn't work for some reasons. We also need to
  //be able to reset the model due to dynamic translations

  function reset() {
    model.clear()

    model.append({
      "company":"WEBLINK",
      "position":qsTr("Qt/QML developer"),
      "date":qsTr("Since") + " 2013",
      "description":qsTr("Unified communicator based on <b>REST</b> api and <b>pjsip</b> library. Currently supports sennheiser, jabra and plantronics headsets. The whole UI created with QML, C++ backend. <b>Responsibility for desktop client</b>")
    })

    model.append({
      "company":"DYNAMICPERCEPTION",
      "position":qsTr("Qt/QML developer"),
      "date":"2014",
      "description":qsTr("Motions creator designed to work with motion controller (serial port) - device that uses arduino to controll camera position and exposure. <b>Responsibility for desktop version of the application</b>")
    })

    model.append({
      "company":"ITL",
      "position":qsTr("Phaser.io/Typescript developer"),
      "date":"2017",
      "description":qsTr("A game that uses phaser.io framework, working with Orbital games manager. Similar to one-armed bandit. <b>Responsibility for UI part</b>")
    })

    model.append({
      "company":"ITL",
      "position":qsTr("Qt/QML developer"),
      "date":"2015",
      "description":qsTr("Orbital - Casinos system and games manager. <b>Responsibility for UI and data models</b>")
    })

    model.append({
      "company":"ITL",
      "position":qsTr("Qt/QML developer"),
      "date":"2016",
      "description":qsTr("Sweetz - A game working with Orbital games manager. Similar to one-armed bandit. <b>Responsibility for QML part</b>")
    })

    model.append({
      "company":"STARFISH",
      "position":qsTr("Qt/QML developer"),
      "date":qsTr("Since") + " 2015",
      "description":qsTr("Application based on REST api for Large-Scale file management. Offers data visualisation, files movement, reporting and files processing. <b>Responsibility for desktop application</b>")
    })

    model.append({
      "company":"TRANSFERSOFT",
      "position":qsTr("Qt/QML developer"),
      "date":"2013",
      "description":qsTr("HyperTransfer application that provides an efficient way for moving data between systems and around the globe. Uses REST api. <b>Responsibility for desktop client application</b>")
    })

    model.append({
      "company":"DYNASPHERE",
      "position":qsTr("Qt/QML developer"),
      "date":"2013",
      "description":qsTr("denkmappBE - mobile application prepared for iOS and Android.")
    })

    model.append({
      "company":"PULSE",
      "position":qsTr("Qt/QML developer"),
      "date":"2013",
      "description":qsTr("A set of applications created in QML (QWidgets previously) that fully controlls drill rigs. <b>Responsibility for UI and data models</b>")
    })

    model.append({
      "company":"LODESTAR EQUIPMENT",
      "position":qsTr("Qt/QML developer"),
      "date":"2013",
      "description":qsTr("A set of applications created in QML that fully controlls drill rigs. <b>Responsibility for UI and data models</b>")
    })

    model.append({
      "company":"",
      "position":qsTr("Qt/QML developer"),
      "date":"2013",
      "description":qsTr("Wallet application for <b>bitmonero</b> cryptocurrency.")
    })

    model.append({
      "company":"",
      "position":qsTr("Qt/C++ developer"),
      "date":"2013",
      "description":qsTr("InSight - application designed to manage waste tanks flow, based on QWidgets only")
    })
  }
}
