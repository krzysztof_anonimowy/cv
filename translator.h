#ifndef TRANSLATOR_H
#define TRANSLATOR_H

#include <QObject>
#include <QTranslator>

class translator : public QObject
{
  Q_OBJECT
  Q_PROPERTY(QString trString READ trString NOTIFY trStringChanged)

public:
  explicit translator(QObject *parent = nullptr);
  Q_INVOKABLE void changeLanguage(const QString &lang);
  QString trString() const { return QString(); }

private:
  QTranslator *m_pPlTranslator;

signals:
  void trStringChanged();
};

#endif // TRANSLATOR_H
