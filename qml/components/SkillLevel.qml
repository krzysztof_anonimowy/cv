import QtQuick 2.4
import "qrc:///qml/components" as CWorks

Row {
  spacing: 4
  property string text: ""
  property int level: 1

  CWorks.Text {
    anchors.verticalCenter: parent.verticalCenter
    text: parent.text
    width: 100
  }

  FontIcon {
    anchors.verticalCenter: parent.verticalCenter
    icon: "star"
    color: level >= 1 ? "#B2C859" : "#CCCCCC"
  }

  FontIcon {
    anchors.verticalCenter: parent.verticalCenter
    icon: "star"
    color: level >= 2 ? "#B2C859" : "#CCCCCC"
  }

  FontIcon {
    anchors.verticalCenter: parent.verticalCenter
    icon: "star"
    color: level >= 3 ? "#B2C859" : "#CCCCCC"
  }

  FontIcon {
    anchors.verticalCenter: parent.verticalCenter
    icon: "star"
    color: level >= 4 ? "#B2C859" : "#CCCCCC"
  }

  FontIcon {
    anchors.verticalCenter: parent.verticalCenter
    icon: "star"
    color: level >= 5 ? "#B2C859" : "#CCCCCC"
  }
}
