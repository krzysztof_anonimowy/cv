import QtQuick 2.4

ListModel {
  id: model
  Component.onCompleted: reset()

  //ListElement won't allow us to use javascript functions.
  //so we won't be able to translate these texts in common way
  //QT_TR_NOOP doesn't work for some reasons. We also need to
  //be able to reset the model due to dynamic translations

  function reset() {
    model.clear()

    model.append({
      "college":qsTr("Wroclaw\nUniversity of\nTechnology"),
      "date":"2007-2011",
      "specialization":qsTr("Faculty of Electronics. Specialization: Information and communications technology"),
      "direction":qsTr("Information and communications technology")
    })

    model.append({
      "college":qsTr("Wroclaw\nUniversity of\nTechnology"),
      "date":"2011-2014",
      "specialization":qsTr("Faculty of Computer Science and Management. Specialization: Information systems"),
      "direction":qsTr("Informatics")
    })
  }
}
