<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl_PL">
<context>
    <name>Basic</name>
    <message>
        <location filename="../Basic.qml" line="35"/>
        <source>Hi, my name is Krzysztof Zając, I&apos;m 30 years old software developer, currently living in Wrocław, Poland. I have a good experience of system and network programming and development of cross platform applications mostly based on Qt framework. I love QML with C++ but sometimes I&apos;m using other languages and technologies as well. I&apos;m opened for new interesting solutions and I like solving unusual problems</source>
        <translation>Witam, nazywam się Krzysztof Zając, jestem 30 letnim programistą aktualnie mieszkajacym we Wrocławiu (Polska). Posiadam spore doświadczenie w programowaniu aplikacji multiplatformowych w większości przypadków bazujących na bibliotece Qt. Uwielbiam QML oraz C++ lecz czasami korzystam z innych języków programowania oraz technologii. Jestem otwarty na interesujące rozwiązania i lubię rozwiązywać nietypowe problemy.</translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <location filename="../Details.qml" line="163"/>
        <source>SKILLS</source>
        <translation>UMIEJĘTNOŚCI</translation>
    </message>
    <message>
        <location filename="../Details.qml" line="208"/>
        <source>INTERESTS</source>
        <translation>ZAINTERESOWANIA</translation>
    </message>
    <message>
        <source>openCV</source>
        <translation type="obsolete">openCV</translation>
    </message>
    <message>
        <location filename="../Details.qml" line="220"/>
        <source>games developing</source>
        <translation>programowanie gier</translation>
    </message>
</context>
<context>
    <name>EducationList</name>
    <message>
        <location filename="../EducationList.qml" line="14"/>
        <location filename="../EducationList.qml" line="21"/>
        <source>Wroclaw
University of
Technology</source>
        <translation>Politechnika\nWrocławska</translation>
    </message>
    <message>
        <location filename="../EducationList.qml" line="16"/>
        <source>Faculty of Electronics. Specialization: Information and communications technology</source>
        <translation>Wydział Elektroniki, Specjalizacja: Teleinformatyka</translation>
    </message>
    <message>
        <location filename="../EducationList.qml" line="17"/>
        <source>Information and communications technology</source>
        <translation>Informatyka i zarządzanie</translation>
    </message>
    <message>
        <location filename="../EducationList.qml" line="23"/>
        <source>Faculty of Computer Science and Management. Specialization: Information systems</source>
        <translation>Wydział Informatyki i Zarządzania. Specjalizacja: Systemy informacyjne</translation>
    </message>
    <message>
        <location filename="../EducationList.qml" line="24"/>
        <source>Informatics</source>
        <translation>Informatyka</translation>
    </message>
</context>
<context>
    <name>ProjectsList</name>
    <message>
        <location filename="../ProjectsList.qml" line="15"/>
        <location filename="../ProjectsList.qml" line="22"/>
        <location filename="../ProjectsList.qml" line="36"/>
        <location filename="../ProjectsList.qml" line="43"/>
        <location filename="../ProjectsList.qml" line="50"/>
        <location filename="../ProjectsList.qml" line="57"/>
        <location filename="../ProjectsList.qml" line="64"/>
        <location filename="../ProjectsList.qml" line="71"/>
        <location filename="../ProjectsList.qml" line="78"/>
        <location filename="../ProjectsList.qml" line="85"/>
        <source>Qt/QML developer</source>
        <translation>Programista Qt/QML</translation>
    </message>
    <message>
        <location filename="../ProjectsList.qml" line="16"/>
        <location filename="../ProjectsList.qml" line="51"/>
        <source>Since</source>
        <translation>Od</translation>
    </message>
    <message>
        <location filename="../ProjectsList.qml" line="17"/>
        <source>Unified communicator based on &lt;b&gt;REST&lt;/b&gt; api and &lt;b&gt;pjsip&lt;/b&gt; library. Currently supports sennheiser, jabra and plantronics headsets. The whole UI created with QML, C++ backend. &lt;b&gt;Responsibility for desktop client&lt;/b&gt;</source>
        <translation>Unified communicator w oparciu o &lt;b&gt;REST&lt;/b&gt; api oraz bibliotekę &lt;b&gt;pjsip&lt;/b&gt;. Aktualnie wspiera zestawy słuchawkowe takie jak sennheiser, jabra lub plantronics. Całość interfaceu wykonana w oparciu o QML. Backend pisany w języku C++. &lt;b&gt;W projekcie byłem odpowiedzialny za całą aplikację desktopową.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../ProjectsList.qml" line="24"/>
        <source>Motions creator designed to work with motion controller (serial port) - device that uses arduino to controll camera position and exposure. &lt;b&gt;Responsibility for desktop version of the application&lt;/b&gt;</source>
        <translation>Kreator ruchu zaprojektowany do współpracy z urządzeniem działającym pod kontrolą arduino, przeznaczony do sterowania pozycją oraz stanem kamery/aparatu. &lt;b&gt;W projekcie byłem odpowiedzialny za aplikację desktopową.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../ProjectsList.qml" line="29"/>
        <source>Phaser.io/Typescript developer</source>
        <translation>Programista Phaser.io/Typescript</translation>
    </message>
    <message>
        <location filename="../ProjectsList.qml" line="31"/>
        <source>A game that uses phaser.io framework, working with Orbital games manager. Similar to one-armed bandit. &lt;b&gt;Responsibility for UI part&lt;/b&gt;</source>
        <translation>Gra wykorzystująca framework phaser.io, działająca z systemem Orbital. Podobna w działaniu do jednorękiego bandyty. &lt;b&gt;W projekcie jestem odpowiedzialny za programowanie interfaceów użytkownika oraz animacji&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../ProjectsList.qml" line="38"/>
        <source>Orbital - Casinos system and games manager. &lt;b&gt;Responsibility for UI and data models&lt;/b&gt;</source>
        <translation>Orbital -aplikacja do zarządzania grami oraz ustawieniami systemowymi. &lt;b&gt;W projekcie odpowiedzialny byłem za programowanie interfaceu użytkownika oraz modeli danych&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../ProjectsList.qml" line="45"/>
        <source>Sweetz - A game working with Orbital games manager. Similar to one-armed bandit. &lt;b&gt;Responsibility for QML part&lt;/b&gt;</source>
        <translation>Sweetz - Gra działająca pod kontrolą systemu Orbital. Podobna w działaniu do jednorękiego bandyty. &lt;b&gt;W projekcie byłem odpowiedzialny za programowanie interfaceu użytkownika oraz animacji z wykorzystaniem QML&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../ProjectsList.qml" line="52"/>
        <source>Application based on REST api for Large-Scale file management. Offers data visualisation, files movement, reporting and files processing. &lt;b&gt;Responsibility for desktop application&lt;/b&gt;</source>
        <translation>Aplikacja bazująca na RESET api służąca do wielkoskalowego zarządzania plikami. Oferuje wizualizacje danych, przenoszenie i przetwarzanie plików oraz raportowanie. &lt;b&gt;W projekcie odpowiedzialny byłem za aplikację desktopową (klient)&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../ProjectsList.qml" line="59"/>
        <source>HyperTransfer application that provides an efficient way for moving data between systems and around the globe. Uses REST api. &lt;b&gt;Responsibility for desktop client application&lt;/b&gt;</source>
        <translation>HyperTransfer - aplikacja, która umożliwia efektywne przenoszenie danych pomiędzy systemami z wykorzystaniem REST api. &lt;b&gt;W projekcie odpowiedzialny za aplikację desktopową (klient)&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../ProjectsList.qml" line="66"/>
        <source>denkmappBE - mobile application prepared for iOS and Android.</source>
        <translation>denkmappBE - aplikacja mobilna przygotowana na system iOS oraz Android.</translation>
    </message>
    <message>
        <location filename="../ProjectsList.qml" line="73"/>
        <source>A set of applications created in QML (QWidgets previously) that fully controlls drill rigs. &lt;b&gt;Responsibility for UI and data models&lt;/b&gt;</source>
        <translation>Zestaw aplikacji stworzonych z wykorzystaniem QML (pierwotnie QWidgets) mające na celu kontrolę wiertnic. &lt;b&gt;Odpowiedzialny za programowanie interfaceu użytkownika oraz modeli danych&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../ProjectsList.qml" line="80"/>
        <source>A set of applications created in QML that fully controlls drill rigs. &lt;b&gt;Responsibility for UI and data models&lt;/b&gt;</source>
        <translation>Zestaw aplikacji stworzonych z wykorzystaniem QML  mające na celu kontrolę wiertnic. &lt;b&gt;Odpowiedzialny za programowanie interfaceu użytkownika oraz modeli danych&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../ProjectsList.qml" line="87"/>
        <source>Wallet application for &lt;b&gt;bitmonero&lt;/b&gt; cryptocurrency.</source>
        <translation>Aplikacja będąca portfelem kryptowaluty &lt;b&gt;bitmonero&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../ProjectsList.qml" line="92"/>
        <source>Qt/C++ developer</source>
        <translation>Programista Qt/C++</translation>
    </message>
    <message>
        <location filename="../ProjectsList.qml" line="94"/>
        <source>InSight - application designed to manage waste tanks flow, based on QWidgets only</source>
        <translation>InSight - aplikacja wykorzystująca QWidgets, zaprojektowana do zarządzana zbiornikiem na odpady.</translation>
    </message>
</context>
</TS>
