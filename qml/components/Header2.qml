import QtQuick 2.4

Text {
  color: "#B2C859"
  font.capitalization: Font.AllUppercase
  font.family: "Arial"
  font.pixelSize: 13
  font.bold: true
  wrapMode: Text.Wrap
}
