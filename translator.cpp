#include "translator.h"
#include <QCoreApplication>

translator::translator(QObject *parent) : QObject(parent) {
  m_pPlTranslator = new QTranslator(this);
}

void translator::changeLanguage(const QString &lang) {
  if(lang == "pl") {
    m_pPlTranslator->load(":/translations/Polish.qm");
    qApp->installTranslator(m_pPlTranslator);
  } else if(lang == "en") {
    qApp->removeTranslator(m_pPlTranslator);
  }

  emit trStringChanged();
}
