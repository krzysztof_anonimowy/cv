import QtQuick 2.4
import "qrc:///qml/components" as CWorks

Rectangle {
  width: 256
  color: "#D7D9D8"

  Column {
    anchors.top: parent.top
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.margins: 20
    spacing: 20

    CWorks.Header1 { //name/surname text
      anchors.left: parent.left
      anchors.right: parent.right
      font.pixelSize: 35
      horizontalAlignment: Text.AlignHCenter
      text: "KRZYSZTOF ZAJĄC"
    } //end name/surname text

    Image { //foto image
      anchors.left: parent.left
      anchors.right: parent.right
      width: parent.width
      height: width
      source: "qrc:///images/face.png"
    } //end foto image

    CWorks.Text {
      anchors.left: parent.left
      anchors.right: parent.right
      horizontalAlignment: Text.AlignJustify
      text: qsTr("Hi, my name is Krzysztof Zając, I'm 30 years old software developer, " +
            "currently living in Wrocław, Poland. I have a good experience of system " +
            "and network programming and development of cross platform applications mostly based " +
            "on Qt framework. I love QML with C++ but sometimes I'm using other languages and " +
            "technologies as well. I'm opened for new interesting solutions and I like solving unusual problems") + Translator.trString
    }
  }

  Column {
    anchors.bottom: parent.bottom
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.margins: 20
    spacing: 20

    CWorks.Category {
      anchors.left: parent.left
      anchors.right: parent.right
      center: true
      icon: "phone"
    }

    Column {
      anchors.left: parent.left
      anchors.right: parent.right

      CWorks.Header2 {
        anchors.left: parent.left
        anchors.right: parent.right
        horizontalAlignment: Text.AlignHCenter
        text: "785 682 173"
      }

      CWorks.Header1 {
        anchors.left: parent.left
        anchors.right: parent.right
        horizontalAlignment: Text.AlignHCenter
        text: "KRZYSZTOF@CWORKS.PL"
      }

      CWorks.Header1 {
        anchors.left: parent.left
        anchors.right: parent.right
        horizontalAlignment: Text.AlignHCenter
        text: "SKYPE: z.krzysztoff7"
      }
    }

    CWorks.VerticalLine {
      anchors.left: parent.left
      anchors.right: parent.right
    }

    Row {
      anchors.horizontalCenter: parent.horizontalCenter
      spacing: 5

      CWorks.ToolButton {
        id: plButton
        source: "qrc:///images/pl.png"
        onClicked: {
          ukButton.checked = false
          Translator.changeLanguage("pl")
        }
      }

      CWorks.ToolButton {
        id: ukButton
        source: "qrc:///images/uk.png"
        checked: true
        onClicked: {
          plButton.checked = false
          Translator.changeLanguage("en")
        }
      }
    }

    CWorks.VerticalLine {
      anchors.left: parent.left
      anchors.right: parent.right
    }
  }
}
