#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "translator.h"

int main(int argc, char *argv[])
{
  QGuiApplication app(argc, argv);

  QQmlApplicationEngine e;
  translator t;
  e.rootContext()->setContextProperty("Translator", static_cast<QObject*>(&t));

  e.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));
  if (e.rootObjects().isEmpty())
    return -1;

  return app.exec();
}
